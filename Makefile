install:
	docker-compose -f docker-compose.client.builder.yml run --rm install
	docker-compose -f docker-compose.server.builder.yml run --rm install

clean:
	docker-compose -f docker-compose.client.builder.yml run --rm clean

start:
	docker-compose up


setup:
	docker volume create nodemodules
	docker volume create virtualenv
	docker volume create bundoxdata 
	docker volume create bundoxelastic
	docker-compose -f docker-compose.server.builder.yml run --rm setup 

destroy:
	docker-compose down
	docker volume rm virtualenv    || true
	docker volume rm nodemodules   || true
	docker volume rm bundoxdata    || true
	docker volume rm bundoxelastic || true

sampledata:
	docker exec -i -t bundox-server /bin/bash -c ". /opt/venv/bin/activate && bundox -vvv docset add --name React --version 16.9.0 --es_addr elasticsearch:9200 --data /opt/bundox/ --force /opt/sample-data/docsets/React.tgz"
	docker exec -i -t bundox-server /bin/bash -c ". /opt/venv/bin/activate && bundox -vvv stackoverflow import-dump --es_addr elasticsearch:9200 --dump_path /opt/sample-data/stackoverflow/"

sampledataonline:
	docker exec -i -t bundox-server /bin/bash -c ". /opt/venv/bin/activate && bundox -vvv docset add --name C++ --version 60 --es_addr elasticsearch:9200 --data /opt/bundox/ --force http://sanfrancisco.kapeli.com/feeds/C++.tgz"

sampledata2:
	docker exec -i -t bundox-server /bin/bash -c ". /opt/venv/bin/activate && bundox -vvv docset add --name Python --version 3.6.5 --es_addr elasticsearch:9200 --data /opt/bundox/ --force /opt/sample-data/temp/Python_3.6.5.tgz"