Expression
  = _* terms:(Term (_ / isolated)*)* isolated* {
  	  return terms.map(function(t) { return t[0]; });
  }

Term
  = term:(Tag / Phrase / Keyword) { return term; }
  / '+'+  term:(Tag / Phrase / Keyword) { return term; }
  / '-'+ term:(Tag / Phrase / Keyword) { term.type = 'excluded_'+term.type; return term ; }

Tag
  = '[' tag:TagChars* ']' { 
  	var type = 'tag';
    tag = tag.join('')

    // Special tag with preceeding "docset"
    const prefix = 'docset'
    const normalized = tag.trim().toLowerCase();
  	if ( tag.length > prefix.length + 1 && normalized.split(':')[0] === prefix ) {
       type = 'docset_tag'
       tag = tag.substring('docset'.length + 1)
    }
  	return {
      type: type,
      value: tag
    }; 
}

TagChars
  = !('[' / ']') char:. { return char; }

Keyword
  = !('-' / '+') chars:KeywordCharacter+ { return {'type': 'keyword', 'value': chars.join('')}; }

KeywordCharacter
  = !(_) char:. { return char; }
  
Phrase
  = !('-' / "+") phrase:QuotedString { return {'type': 'phrase', 'value': phrase}; }

QuotedString
  = '"' chars:DoubleStringCharacter* '"' { return chars.join(''); }
  / "'" chars:SingleStringCharacter* "'" { return chars.join(''); }

DoubleStringCharacter
  = !('"' / "\\") char:. { return char; }
  / "\\" sequence:EscapeSequence { return sequence; }

SingleStringCharacter
  = !("'" / "\\") char:. { return char; }
  / "\\" sequence:EscapeSequence { return sequence; }

EscapeSequence
  = "'"
  / '"'
  / "\\"
  / "b"  { return "\b";   }
  / "f"  { return "\f";   }
  / "n"  { return "\n";   }
  / "r"  { return "\r";   }
  / "t"  { return "\t";   }
  / "v"  { return "\x0B"; }

isolated 
 = '-'+ _+
 / '+'+ _+

_ "whitespace"
 = [ \n\r\t]+

 