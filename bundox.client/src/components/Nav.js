
import React from "react";
import { Card, Heading } from "evergreen-ui";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStackOverflow } from '@fortawesome/fontawesome-free-brands';
import { faBook } from '@fortawesome/pro-light-svg-icons';
import { Link } from 'react-router-dom'
import styled from 'styled-components';

const NavLogo = props => {
  return (
    <Card
      display="flex"
      flexGrow={1}
      flexShrink={9999}
      minWidth={40}
      paddingLeft={20}
      alignItems="center"
    >
      <Link to="/" style={{ width: "100%", textDecoration: "none" }}>
        <Card
          display="flex"
          alignItems="center"
          fontFamily="Inter"
          color="black"
          textDecoration="none"
        >
          <Heading size={700} color="#333">
            <img height="50" src="/img/bundox-logo-nav.png" />
          </Heading>
        </Card>
      </Link>
    </Card>
  );
};

const NavLinksContainer = styled.div`
  display: flex;
  flex-grow: 1;
  flex-shrink: 9999;
  min-width: 40px;
  align-items: center;
  justify-content: flex-end;

  a {
    padding-right: 20px;
  }
`;

const NavLinks = props => {

  return (
    <NavLinksContainer>
      <Link to="/docsets" style={{ paddingLeft: "20px" }} title="Search Docsets">
        <FontAwesomeIcon icon={faBook} size="2x" color="#333" />
      </Link>
      <Link to="/stackoverflow" style={{ paddingLeft: "20px" }} title="Search StackOverflow Questions" >
        <FontAwesomeIcon icon={faStackOverflow} size="2x" color="#333" />
      </Link>
    </NavLinksContainer>
  );
};

const OuterNavContainer = styled.nav`
  display: flex;
  flex-grow: 1;
  flex-shrink: 0;
  align-items: center;
  position: relative;
  justify-content: center;
  width: 100%;
  flex-direction: column;
  order: 0;
  border-bottom: 1px solid rgba(0,0,0,.0975);
`;

const InnerNavContainer = styled(OuterNavContainer)`
  flex-direction: row;
  height: 77px;
  border: 0px;
`;

function Nav() {
  return (
    <OuterNavContainer>
      <InnerNavContainer>
        <NavLogo />
        <NavLinks />
      </InnerNavContainer>
    </OuterNavContainer>
  );
}

export default Nav;