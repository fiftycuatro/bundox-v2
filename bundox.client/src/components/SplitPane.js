import React, { useState } from 'react';
import { default as ReactSplitPane } from 'react-split-pane';
import styled from 'styled-components';

export const LeftPane = ({ children }) => <React.Fragment>{children}</React.Fragment>
export const RightPane = ({ children }) => <React.Fragment>{children}</React.Fragment>

const StyledSplitPane = styled(ReactSplitPane)`
.Resizer {
  box-sizing: border-box;
  background: #000;
  opacity: 0.5;
  z-index: 1;
  background-clip: padding-box;
}

.Resizer:hover {
  -webkit-transition: all 2s ease;
  transition: all 2s ease;
}

.Resizer.horizontal {
  height: 11px;
  margin: -5px 0;
  border-top: 5px solid rgba(255, 255, 255, 0);
  border-bottom: 5px solid rgba(255, 255, 255, 0);
  cursor: row-resize;
  width: 100%;
}

.Resizer.horizontal:hover {
  border-top: 5px solid rgba(0, 0, 0, 0.5);
  border-bottom: 5px solid rgba(0, 0, 0, 0.5);
}

.Resizer.vertical {
  width: 11px;
  margin: 0 -5px;
  border-left: 5px solid rgba(255, 255, 255, 0);
  border-right: 5px solid rgba(255, 255, 255, 0);
  cursor: col-resize;
}

.Resizer.vertical:hover {
  border-left: 5px solid rgba(0, 0, 0, 0.5);
  border-right: 0px solid rgba(0, 0, 0, 0.5);
}

`;
const SplitPaneContentWrapper = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
  overflow-x: hidden;
  overflow-y: auto;
`;

const SplitPaneDragOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
  z-index: 10;
  display: ${({ isDragging }) => isDragging ? "block" : "none"};
`;

/* SplitPane required some additional wrappers to allow for IFrames
   to be in any of the child panes. Basically we display a overlay
   div when we are dragging and resizing the split. Once the drag
   is finished we hide the overlay div */
const SplitPane = ({ children, ...props }) => {
  const [isDragging, setDragging] = useState(false);

  const child1 = children.length > 0 ? children[0] : <div />,
    child2 = children.length > 1 ? children[1] : <div />;

  return (
    <StyledSplitPane
      onDragStarted={() => setDragging(true)}
      onDragFinished={() => setDragging(false)}
      {...props}
    >
      <SplitPaneContentWrapper>
        {child1}
        <SplitPaneDragOverlay isDragging={isDragging} />
      </SplitPaneContentWrapper>
      <SplitPaneContentWrapper>
        {child2}
        <SplitPaneDragOverlay isDragging={isDragging} />
      </SplitPaneContentWrapper>
    </StyledSplitPane>
  );
}

export default SplitPane;