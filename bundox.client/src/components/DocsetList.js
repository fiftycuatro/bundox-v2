import React, { useContext } from 'react';
import { Link } from 'react-router-dom'
import { Pane, Text } from 'evergreen-ui';
import { groupBy } from 'lodash';
import SiteContext from "../context/site-context";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearchPlus } from '@fortawesome/pro-light-svg-icons';
import styled from 'styled-components';
import { useSearch } from '../context/search'

const DocsetLink = styled(({ active, ...props }) => <Link {...props} />)`
    text-decoration: none;
    display: flex;
    flex-grow: 1;
    align-items: center;
`;

const DocsetVersionLink = ({ name, version, path, updateSearch }) => {
    return (
        <Link
            to={`/docsets/documentation/${name}/${version}/${path}`}
            onClick={() => updateSearch(`[${name}@${version}] `)}>
            <Text color="#aaa" size={300}>{version}</Text>
        </Link>
    );
}

const NameToIcon = { "c++": "cplusplus" }

const DocsetListRow = styled(({ active, ...props }) => <div {...props} />)`
    display: flex;
    align-items: center;
    padding: 5px; 
    border-bottom: 1px solid rgba(0,0,0,.0975);
    background: ${({ active }) => active ? "#feb20147" : "inherit"}
`;

const DocsetList = ({ location, match }) => {
    const { searchBarRef, docsets } = useContext(SiteContext);
    const { searchInput } = useSearch();

    const addDocsetToSearch = name => searchInput.setValue(`${searchInput.value} [docset:${name}]`)

    // Group docset by family. 
    const groupDocsets = groupBy(docsets, (i) => i.name);

    return (
        <Pane>
            {Object.keys(groupDocsets).map((name) => {
                const ds = groupDocsets[name];
                const latest = ds.sort((d) => d.version).reverse()[0];
                const icon = NameToIcon[name.toLowerCase()] || name;
                //const active = match.params.name === name
                const active = location.pathname.startsWith(`/docsets/documentation/${name}/${latest.version}/`) || undefined

                return (
                    <DocsetListRow
                        key={name}
                        active={active} >
                        <DocsetLink
                            to={`/docsets/documentation/${name}/${latest.version}/${latest.indexpath}`}
                        >
                            <img style={{ marginRight: 5 }} width={25} src={`/img/icons/${icon}/${icon}-original.svg`} />
                            <Text style={{ flexGrow: 1 }} size={600}>{name}</Text>
                        </DocsetLink>
                        <a onClick={() => addDocsetToSearch(name)}>
                            <FontAwesomeIcon icon={faSearchPlus} color="#333" />
                        </a>
                    </DocsetListRow>
                );
            })}
        </Pane >
    );
}

export default DocsetList;