import React, { useState } from 'react';
import styled from 'styled-components';

// Known mappings
const NameToIcon = {
    "c++": "cplusplus"
}

const DocIconImg = styled.img`
    display: ${({ visible }) => visible ? "inherited" : "none"};
`;

const DocIcon = ({ name, ...props }) => {
    const [visible, setVisible] = useState(false);
    const icon = NameToIcon[name.toLowerCase()] || name;

    return (
        <DocIconImg
            visible={visible}
            src={`/img/icons/${icon}/${icon}-original.svg`}
            onLoad={() => { setVisible(true) }}
            {...props} />
    );
};

export default DocIcon;