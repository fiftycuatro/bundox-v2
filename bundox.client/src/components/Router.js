import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import FeedPage from '../pages/Feed';
import DocsetPage from '../pages/Docset';

const Router = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path='/docsets' component={DocsetPage} />
                <Route path='/' component={FeedPage} />
            </Switch>
        </BrowserRouter>
    );
};

export default Router;