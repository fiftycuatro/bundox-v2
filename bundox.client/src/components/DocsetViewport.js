import React, { useState, useEffect } from 'react'
import { matchPath } from 'react-router';
import styled from 'styled-components';

const IFrame = styled.iframe`
    width: 100%;
    height: calc( 100vh - 76px );
`;

export default function DocsetViewport({ location, match, history }) {
    var iframeRef;

    const { name, version, path } = match.params;
    const { hash, search } = location;

    // For some reason the proxy doesnt work here
    const url = `/api/v1/documents/${name}/${version}/documentation/${path}${hash}${search}`

    function onClick(e) {
        // TODO: Handle Cross domain
        // Notice that we use currentTarget. The event may originate
        // from something that is not a link. In this case we continue
        // to allow the event to propagate until we hit a currentTarget
        // thats has a href. Since we attached this method to all links
        // in the document we should always find something that has a href
        // if not the URL history will just not work 
        if (e.currentTarget.href) {
            const url = new URL(e.currentTarget.href)
            const path = `${url.pathname}${url.hash}${url.search}`
            const match = matchPath(path, {
                path: '/api/v1/documents/:name/:version/documentation/:path+'
            });

            if (match) {
                const { name, version, path } = match.params
                history.push(`/docsets/documentation/${name}/${version}/${path}`)

                // Prevent navigation if we are going to let the history
                // trigger a redraw
                e.preventDefault();
                return false;
            }
        }
        return true;
    }

    function onLoad(e) {
        var ref = iframeRef;
        const links = ref.contentWindow.document.links;
        for (var i = 0; i < links.length; i++) {
            if (links[i].host !== iframeRef.contentWindow.location.host) {
                // Don't even bother opening cross domain content in this iframe
                // just open in new tab/window
                links[i].target = "_blank";
            }
            else {
                links[i].addEventListener('click', onClick)
            }
        }
    }

    return (
        <IFrame ref={(r) => iframeRef = r} src={url} onLoad={onLoad} frameBorder={0} />
    );
}