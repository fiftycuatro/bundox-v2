import React, { useState, useEffect } from 'react';
import { useFormInput, useDebounce } from '../hooks';
import { parse } from '../searchParser'
import styled from 'styled-components';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimesCircle } from '@fortawesome/pro-solid-svg-icons';
import DocIcon from '../components/DocIcon';
import { useSearch } from '../context/search'

const InputContainer = styled.div`
    display: flex;
    align-items: center;
    background:#ddd;
    width: 100%;

    input {
        height:30px;
        flex-grow: 1;
    }

    .filter-badge {
        height: 24px;

        display: flex;
        align-items: center;

        margin: 0px 5px;
        padding: 5px;
        border-style: solid;
        border-width: 1px;
        border-color: #ccc;
        border-radius: 5px;
        background: white;

        font-size: .75em;
    }

    .filter-icon-wrapper {
        cursor: pointer
    }

    .filter-icon {
        color: #aaa;
        margin-left: 2px;
    }

    img {
        height:24px;
        margin-right: 2px;
    }
`;

// This function looks for tags that are prefixed with
// docset: or is stackoverflow. 
const getKnownTags = (searchInput) => {
    var knownTags = []
    var tags;
    try { tags = parse(searchInput).filter((s) => s.type === 'tag') } catch { return []; }

    tags.forEach(({ value }) => {
        if (value.trim().toLowerCase() === "stackoverflow") {
            knownTags.push({ tag: "stackoverflow", orig: value });
        }
        else if (value.split(":")[0].trim().toLowerCase() === "docset") {
            var tag = value.replace(`${value.split(":")[0]}:`, "")
            knownTags.push({ tag: tag.trim().toLowerCase(), orig: value });
        }
    })
    return knownTags;
};

const endspace = s => {
    var es = ''
    for (var i = s.length - 1; i >= 0; i--)
        if (s.charAt(i) === ' ')
            es += ' '
    return es
}

const SearchBar = React.forwardRef(({ onSearch, onSearchClear, ...props }, ref) => {

    const { searchInput, searchTokens } = useSearch();
    const debouncedSearchInput = useDebounce(searchInput.value, { minLength: 3 });

    const tags = searchTokens
        .filter(token => token.type === 'docset_tag')
        .map(token => token.value)

    return (
        <InputContainer>
            {tags.map((tag) =>
                <div key={tag} className="filter-badge">
                    <DocIcon name={tag.split("@")[0]} />
                    <span>{tag}</span>
                    <div
                        className="filter-icon-wrapper">
                        <FontAwesomeIcon
                            icon={faTimesCircle}
                            className="filter-icon"
                        />
                    </div>
                </div>
            )}
            <input
                type="text"
                ref={ref}
                {...searchInput.bind}
                {...props} />
        </InputContainer>
    );
});

export default SearchBar;