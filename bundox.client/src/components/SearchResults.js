import React, { useContext, useEffect } from 'react';
import DocIcon from '../components/DocIcon'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { useSearch } from '../context/search'

const highlight = (value, search) => {
    var currentPos = 0;
    var lastMatch = -1;
    var result = "";
    search = search || "";
    search = search.toLowerCase();
    var valueLower = value.toLowerCase();
    for (var i = 0; i < search.length; i = i + 1) {
        var c = search.charAt(i);
        var match = valueLower.indexOf(c, currentPos);
        if (match === -1) {
            break;
        } else if (lastMatch + 1 === match) {
            if (lastMatch === -1) {
                result = "<mark>";
            }
            result = result + value.charAt(match);
            lastMatch = match;
            currentPos = currentPos + 1;
        } else {
            if (lastMatch !== -1) {
                result = result + "</mark>";
            }
            result = result + value.substring(lastMatch + 1, match);
            result = result + "<mark>" + value.charAt(match);
            lastMatch = match;
            currentPos = match + 1;
        }
    }
    if (lastMatch + 1 !== value.length) {
        if (lastMatch === -1) {
            result = value
        } else {
            result = result + "</mark>";
            result = result + value.substring(lastMatch + 1, value.length);
        }
    }
    return result;
};

const BadgeContainer = styled.span`
    display: inline-flex;
    justify-content: center;
    align-items: center;
    width: 15px;
    height: 15px;
    margin-right: 2px;
    text-transform: uppercase;
    border-radius: 5px;
    padding: 5px;
    flex-shrink: 0;

    color: white;
    background ${props => props.color || "#19647E"}
`

const typeToColor = {
    "class": "#28AFB0",
    "exception": "#AD2E24",
    "method": "#EE964B",
    "function": "#EE964B",
    "constructor": "#C75146",
    "enum": "#1F271B"
}

const Badge = ({ className, name, ...props }) => {
    const color = typeToColor[name.toLowerCase()] || "#19647E";

    return (
        <BadgeContainer
            color={color}
            className={className}
            {...props}
        >
            {name.charAt(0)}
        </BadgeContainer>
    );
};

const ResultContainer = styled.div`
    display: flex;

    overflow: hidden;
    text-overflow: ellipsis;

    a {
        text-decoration: none;
        color: #425a70;
    }

    mark {
        background: #ffd400;
        color: black;
    }
`;

const SearchResultItem = ({ item: { document, path, subject, type }, searchInput }) => {
    return (
        <ResultContainer>
            <DocIcon name={document.name} height={25} title={`${document.name}@${document.version}`} />
            <Badge name={type} title={type} />
            <Link
                to={`/docsets/documentation/${document.name}/${document.version}/${path}`}>
                <span dangerouslySetInnerHTML={{ __html: highlight(decodeURIComponent(subject), searchInput) }} />
            </Link>
        </ResultContainer>
    );
};

const ResultList = styled.ul`
    list-style-type: none;
    padding: 0px;
`;

const ResultItem = styled(({ active, ...props }) => <li {...props} />)`
    padding-top: 5px;
    border-bottom: 1px solid rgba(0,0,0,.0975);

    background: ${({ active }) => active ? "#feb20147" : "inherit"}
`;

const SearchResults = ({ location }) => {
    const resultKey = (r) => `${r.documentId}_${r.path}_${r.subject}`;
    const { results, searchInput } = useSearch();

    return (
        <ResultList>
            {results.map((r) => {
                const active = `${location.pathname}${location.hash}`.endsWith(`${r.document.name}/${r.document.version}/${r.path}`);
                return <ResultItem active={active} key={resultKey(r)}><SearchResultItem item={r} searchInput={searchInput.value} /></ResultItem>
            })}
        </ResultList>
    )
}

export default SearchResults;