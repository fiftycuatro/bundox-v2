import { useState, useEffect } from 'react';

export const useInput = initialValue => {
  const [value, setValue] = useState(initialValue);

  return {
    value,
    setValue,
    reset: () => setValue(""),
    bind: {
      value,
      onChange: event => {
        setValue(event.target.value);
      }
    }
  };
};

export function useFormInput(initialValue) {
  const [value, setValue] = useState(initialValue);

  function handleChange(e) {
    if (e.target)
      setValue(e.target.value);
    else
      setValue(e);
  }
  return [value, handleChange];
}

// Debounce input allowing for minLengths
export function useDebounce(value, { minLength = 0, delay = 500 }) {
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(() => {
    // Set to empty if minLength is not met
    if (value.length === 0) {
      // Immediately set to empty
      setDebouncedValue('');
      return;
    }
    else if (value.length < minLength) {
      // Don't change current value
      return;
    }

    // Otherwise set value when delay expires
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    // Clear setting if value changes inbetween
    return () => {
      clearTimeout(handler);
    };
  }, [value, delay, minLength]);

  return debouncedValue;
}