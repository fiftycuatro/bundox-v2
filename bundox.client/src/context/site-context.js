import React from 'react';
import LocalStorageService from "./localstorage";

const localStorageService = LocalStorageService.getService();

export const DefaultState = {
    logged_in: localStorageService.getAccessToken() ? true : false,
    user: {
        username: "guest",
        access_token: "",
        refresh_token: ""
    },
    docsets: [],
    search: {
        raw: "",
        data: {}
    },
    docsetSearchResults: undefined,
    searchBarRef: React.createRef()
};

export const SetStateMethods = {
    login: (username, password) => { },
    logout: () => { },
    getAllDocsets: () => { },
    updateDocsetSearch: (text) => { },
    updateStackoverflowSearch: (text) => { },
};

export default React.createContext({ ...DefaultState, ...SetStateMethods });