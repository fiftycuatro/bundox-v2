export const DOCSETS = "DOCSETS";
export const DOCSETS_SEARCH_RESULTS = "DOCSETS_RESULTS";
export const STACKOVERFLOW_SEARCH_RESULTS = "STACKOVERFLOW_SEARCH_RESULTS";

const docsets = ({ docsets }, state) => {
  return { ...state, docsets };
};

const updateDocsetSearchResults = ({ results }, state) => {
  return { ...state, docsetSearchResults: results }
}

export const siteReducer = (state, action) => {
  switch (action.type) {
    case DOCSETS:
      return docsets(action, state);
    case DOCSETS_SEARCH_RESULTS:
      return updateDocsetSearchResults(action, state);
    default:
      return state;
  }
};
