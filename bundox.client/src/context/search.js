import React, { useReducer, useContext, useEffect } from "react";
import { parse } from '../searchParser'
import { useDebounce } from '../hooks'
import { searchDocsetsDocumentation } from '../context/Services'

// Reducer types
const CLEAR = 'clear';
const UPDATE = 'update';
const UPDATE_RESULTS = 'update-results';

export const SearchContext = React.createContext();
export const useSearch = () => useContext(SearchContext);

const endspace = s => {
    if (s === undefined)
        return ''

    var es = ''
    for (var i = s.length - 1; i >= 0; i--)
        if (s.charAt(i) === ' ')
            es += ' '
        else {
            return es
        }
    return es
}

const removeDuplicateTags = tokens => {
    return tokens
        .reduce(({ map, tokens }, token) => {
            const id = `${token.type}:${token.value}`;
            if (!token.type.endsWith('tag'))
                tokens.push(token)
            else if (!map.has(id)) {
                map.add(id)
                tokens.push(token)
            }
            return { map, tokens };
        }, { map: new Set(), tokens: [] }).tokens;
}

const defaultSearchState = {
    value: '',
    tokens: [],
    previousTags: [],
    results: undefined
}

const updateSearch = (state, { value }) => {
    const { previousTags } = state
    var tokens;
    var tags;

    if (value.trim().length === 0)
        return { ...state, results: undefined }

    try {
        tokens = previousTags.concat(parse(value));
        tags = tokens.filter(token => token.type.endsWith('tag'))
    }
    catch (e) {
        tokens = previousTags.concat([{ type: "keyword", value }]);
        tags = tokens.filter(token => token.type.endsWith('tag'))
    }

    // Remove duplicate tags
    tokens = removeDuplicateTags(tokens);
    tags = removeDuplicateTags(tags);

    var taglessInput = tokens
        .filter(token => !token.type.endsWith('tag'))
        .map(token => {
            switch (token.type) {
                case 'keyword':
                    return token.value;
                case 'exclude_keyword':
                    return '-' + token.value;
                case 'phrase':
                    return `"${token.value}"`;
                case 'exclude_phrase':
                    return `-"${token.value}"`;
                default:
                    return token.value;
            }
        })
        .join(' ') + endspace(value);

    // returns token and value
    return { ...state, value: taglessInput, tokens, previousTags: tags }
}

const updateResults = (state, { results }) => {
    if (results.length === 0 && state.value.trim().length === 0)
        return { ...state, results: undefined }
    else
        return { ...state, results }
}

// takes search input parses it 
export const searchReducer = (state, { type, ...args }) => {
    switch (type) {
        case UPDATE:
            return updateSearch(state, args)
        case UPDATE_RESULTS:
            return updateResults(state, args);
        case CLEAR:
        default:
            return defaultSearchState;
    }
}

export const SearchProvider = ({
    children,
    ...options
}) => {
    const [{ value, tokens, results }, dispatch] = useReducer(searchReducer, defaultSearchState);
    const debouncedValue = useDebounce(value, { minLength: 3, ...options })

    const searchInput = {
        value,
        setValue: value => { dispatch({ type: UPDATE, value }) },
        reset: () => { dispatch({ type: CLEAR }) },
        bind: {
            value,
            onChange: e => dispatch({ type: UPDATE, value: e.target.value })
        }
    };

    // On debonced
    useEffect(() => {
        searchDocsetsDocumentation(tokens)
            .then(results => dispatch({ type: UPDATE_RESULTS, results }))
        // We only want to search if the search has debounced
        // eslint-disable-next-line
    }, [debouncedValue])

    return (
        <SearchContext.Provider
            value={{
                searchInput,
                searchTokens: tokens,
                reset: searchInput.reset,
                results: results
            }}>
            {children}
        </SearchContext.Provider>
    );
}
