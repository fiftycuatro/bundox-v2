import axios from "axios";
import { parse } from '../searchParser'

export async function getDocsets(name = null, version = null) {
    const url = "/api/v1/documents";
    const response = await axios.get(url);
    return response.data;
}

export async function searchDocsetsDocumentation(query) {
    // Can't query without a search term
    if (!query)
        return []

    const isTag = (e) => e.type === 'tag';
    if (query.every(isTag))
        return []

    const url = "/api/v2/docsets/search/";
    const response = await axios.post(url, query)
    return response.data;
}