import React, { useReducer } from "react";
import SiteContext, { DefaultState } from "./site-context";
import { siteReducer, DOCSETS, DOCSETS_SEARCH_RESULTS, STACKOVERFLOW_SEARCH_RESULTS } from "./reducers";
import { getDocsets, searchDocsetsDocumentation } from "./Services";

const GlobalState = props => {
  const [state, dispatch] = useReducer(siteReducer, DefaultState);

  const listDocsets = () => {
    getDocsets()
      .then((docsets) => {
        dispatch({ type: DOCSETS, docsets });
      });
  };

  const searchDocsets = (text) => {
    searchDocsetsDocumentation(text)
      .then((results) => {
        dispatch({ type: DOCSETS_SEARCH_RESULTS, results })
      });
  }

  const clearDocsetResults = () => {
    dispatch({ type: DOCSETS_SEARCH_RESULTS, results: undefined })
  }

  return (
    <SiteContext.Provider
      value={{
        ...state,
        getAllDocsets: listDocsets,
        searchDocsets: searchDocsets,
        clearDocsetResults: clearDocsetResults
      }}
    >
      {props.children}
    </SiteContext.Provider>
  );
};

export default GlobalState;
