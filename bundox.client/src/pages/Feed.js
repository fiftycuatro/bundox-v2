import React from 'react';
import Layout from '../components/Layout';
import { Button } from 'evergreen-ui';

const FeedPage = () => {
  return (
      <Layout>
        <Button appearance="primary">Click Me!</Button>
      </Layout>
  );
}

export default FeedPage;