import React, { useContext, useEffect } from 'react';
import { Route } from 'react-router-dom';
import SiteContext from '../context/site-context';
import Layout from '../components/Layout';
import DocsetList from '../components/DocsetList';
import DocsetViewport from '../components/DocsetViewport';
import SplitPane, { LeftPane, RightPane } from '../components/SplitPane';
import SearchBar from '../components/SearchBar';
import styled from 'styled-components';
import SearchResults from '../components/SearchResults';
import { useSearch } from '../context/search'

const SidebarContent = props => {
  const { results } = useSearch();

  if (results && results.length > 0) {
    return <SearchResults {...props} />
  }
  else if (results) {
    return <span>No Results Found...</span>
  }
  else {
    return <DocsetList {...props} />
  }
}

const DocsetSearchBar = styled(SearchBar)`
  height: 30px;
  font-size: 1.15em;
  font-weight: 200;
  width: calc( 100% - 3px );
  height: 30px;
`;

const DocsetPage = (props) => {
  const {
    getAllDocsets,
  } = useContext(SiteContext);

  // TODO: Move to SiteContext
  // eslint-disable-next-line
  useEffect(() => { getAllDocsets() }, [])

  return (
    <Layout>
      <SplitPane split="vertical" defaultSize={300} primary="first">
        <LeftPane>
          <DocsetSearchBar placeholder="Search Docsets..." />
          <Route exact path="/docsets" component={SidebarContent} />
          <Route path="/docsets/documentation/:name/:version" component={SidebarContent} />
        </LeftPane>
        <RightPane>
          <Route exact path="/docsets/documentation/:name/:version" render={() => <h1>AUTO INDEX</h1>} />
          <Route path="/docsets/documentation/:name/:version/:path+" component={DocsetViewport} />
        </RightPane>
      </SplitPane>
    </Layout>
  );
}

export default DocsetPage;