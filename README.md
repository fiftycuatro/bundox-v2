# BUNDOX 
Bundox is an offline documentation repository. It provides a unified 
search experience for a variety of formats

## DEVELOPMENT - GETTING STARTED
Bundox makes use of docker, docker-compose and VSCode to setup a 
development environment. 

### Prequisites

    1. docker
    2. make
    3. Visual Studio Code with the Remote Development extension installed


### Starting Developement Environment
Make is used to run a few tasks to get the development environment up and running. 

```
$ make setup
$ make install
$ make start
```

Navigate to [http://localhost:3000](http://localhost:3000) to view a running bundox system. See below sections for adding sample data.

To destroy the setup run the following.

```
$ make destroy
```

### Starting the Editing Environment

It is recomended to use VSCode's Remote Development extension for development. Open a remote folder __<CTRL+SHIFT+P>__ and type `Remote-Containers:Open Folder In Container...` and select either `bundox.server` or `bundox.client` in this repo. Note that currently two (2) VSCode windows must be used to develop both the client and server parts of the application. Once the remote folder is open editing most files will result in hot reload of the application. 


### Adding Sample Data
An offline set of sample data is provide and installed using the `make sampledata` task. A set of sample data that requires internet connectivity can be installed by running the `make sampledataonline` task