import os
from setuptools import setup, find_packages
here = os.path.abspath(os.path.dirname(__file__))

install_requires = [
    'lxml',
    'crayons',
    'elasticsearch>=6.0.0,<7.0.0',
    'elasticsearch-dsl>=6.0.0,<7.0.0',
    'beautifulsoup4',
    'werkzeug',
    'click',
    'pyYAML',
    'Flask',
    'twisted'
]

tests_require = []

setup(name='bundox',
      version='2.0',
      description='Documentation Server',
      zip_safe=False,
      package_dir={'': 'src'},
      packages=find_packages('src'),
      install_requires=install_requires,
      include_package_data=True,
      tests_require=tests_require,
      entry_points={
          'console_scripts': ['bundox=bundox.cli:cli_main']
      }
      )
