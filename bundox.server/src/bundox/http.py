import time
import logging
import os
import json
import mimetypes
from datetime import datetime

from flask import Blueprint, Flask, request, jsonify, send_from_directory, current_app as app

# TODO: Should go away
from werkzeug.wrappers import Request, Response
from werkzeug.routing import Map, Rule, NotFound
from werkzeug.exceptions import HTTPException, NotFound
from werkzeug.wsgi import SharedDataMiddleware
from werkzeug.utils import redirect
from werkzeug.datastructures import Headers

from utilities import GET, POST, PUT, DELETE, CORSMiddleware, static_file_resource

import docset
import stackoverflow

logger = logging.getLogger(__name__)

bundoxV1Api = Blueprint('bundoxV1Api', __name__)


@bundoxV1Api.after_request
def BundoxV1ApiResponse(response):
    if 'Content-Type' not in response.headers:
        response.headers['Content-Type'] = 'application/bundox.api.v1+json'
    return response


@bundoxV1Api.route('/documents/', methods=['GET'])
@bundoxV1Api.route('/documents/<name>/', methods=['GET'])
@bundoxV1Api.route('/documents/<name>/<version>/', methods=['GET'])
def documents(name=None, version=None):
    es_addr = app.config['ES_ADDR']
    return jsonify(docset.documents(es_addr, name, version)), 200


@bundoxV1Api.route('/documents/<name>/<version>/', methods=['DELETE'])
def delete(name, version):
    es_addr = app.config['ES_ADDR']
    data_path = app.config['DATA_PATH']
    docset.delete(es_addr, data_path, name, version)
    return jsonify({'status': 'success'}), 200


@bundoxV1Api.route('/documents/documentation/', methods=['GET'])
def search():
    es_addr = app.config['ES_ADDR']
    searchTerm = request.args.get('searchTerm', '')
    size = request.args.get('size', 200)
    return jsonify(docset.search(es_addr, searchTerm=searchTerm, size=size)), 200


@bundoxV1Api.route('/documents/<name>/<version>/documentation/<path:pagePath>', methods=['GET'])
def docset_page(name, version, pagePath):
    data_path = app.config['DATA_PATH']
    path = 'docsets/%s.docset/Contents/Resources/Documents/%s' % (
        docset.docset_id(name, version), pagePath)
    guess = mimetypes.guess_type(pagePath)
    mimetype = guess[0] or 'text/plain'
    return send_from_directory(data_path, path), 200, {'Content-Type': mimetype}


bundoxV2Api = Blueprint('bundoxV2Api', __name__)


@bundoxV2Api.after_request
def BundoxV2ApiResponse(response):
    if 'Content-Type' not in response.headers \
            or response.headers['Content-Type'] == 'application/json':
        response.headers['Content-Type'] = 'application/bundox.api.v2+json'
    return response


@bundoxV2Api.route('/questions/')
@bundoxV2Api.route('/questions/<int:id>')
def questions(id=None):
    es_addr = app.config['ES_ADDR']
    return jsonify(stackoverflow.questions(es_addr, id)), 200


@bundoxV2Api.route('/questions/search/')
def search_questions():
    return jsonify(
        stackoverflow.questions(
            es_addr=app.config['ES_ADDR'],
            search=request.args.get('search', ''),
            tags=request.args.getlist('tag'),
            size=request.args.get('size', 50),
            page=request.args.get('page', 1))
    ), 200

def isTag(t): return t['type'] == 'tag'

def index_from_tag(tag, names, indices):
    parts = tag['value'].split('@')
    name = parts[0].lower()
    if len(parts) > 1:
        version = parts[-1].lower()
        index = docset.docset_index(name, version) 
        if index in indices:
            return index
    elif name in names:
        return docset.docset_index(name, "*") 
    return None

@bundoxV2Api.route('/docsets/search/', methods=['POST'])
def search():
    es_addr = app.config['ES_ADDR']
    size = request.args.get('size', 200)
    terms = request.json

    docs = docset.documents(es_addr)
    doc_names = [d['name'].lower() for d in docs]
    doc_indices = [docset.docset_index(d['name'], d['version']) for d in docs]

    indices = []
    for tag in filter(isTag, terms):
        index = index_from_tag(tag, doc_names, doc_indices)
        if index is not None:
            indices.append(index)
    indices = docset.ALL_DOCSETS_INDEX if len(indices) == 0 else indices
    logger.info(indices)

    searchTerms = []
    for tag in terms:
        if tag['type'] in ["keyword", "phrase"]:
            searchTerms.append(tag['value'])
    searchTerm = ' '.join(searchTerms)

    return jsonify(docset.search(es_addr, 
                                 searchTerm=searchTerm, 
                                 indices=indices,
                                 size=size)), 200

static_files = Blueprint('static_files', __name__)


@static_files.route('/')
@static_files.route('/<path:pagePath>')
def serve_static_files(pagePath='index.html'):
    return send_from_directory('static/public', pagePath)


@static_files.route('/favicon.ico')
def serve_favicon():
    return send_from_directory('static/public', 'img/bundox_favicon.png')


@static_files.errorhandler(404)
def serve_index(_):
    return send_from_directory('static/public', 'index.html')


def add_cors_headers(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    if request.method == 'OPTIONS':
        response.headers['Access-Control-Allow-Methods'] = 'DELETE, GET, POST, PUT'
        headers = request.headers.get('Access-Control-Request-Headers')
        if headers:
            response.headers['Access-Control-Allow-Headers'] = headers
    return response


def create_app(host, port, debug, es_addr, data_path):
    app = Flask(__name__)
    app.static_url_path = '/'
    app.static_folder = '/static/public'
    app.config['ES_ADDR'] = es_addr
    app.config['DATA_PATH'] = data_path
    app.after_request(add_cors_headers)
    app.register_blueprint(bundoxV1Api, url_prefix="/api/v1")
    app.register_blueprint(bundoxV2Api, url_prefix="/api/v2")
    app.register_blueprint(static_files)

    def run_app():
        if debug:
            app.run(host=host, port=port, debug=True)
        else:
            from twisted.web.http import proxiedLogFormatter
            from twisted.internet import reactor
            from twisted.web.server import Site
            from twisted.web.wsgi import WSGIResource

            reactor_args = {}
            resource = WSGIResource(reactor, reactor.getThreadPool(), app)
            siteFactory = Site(resource, logPath="http.log")
            reactor.listenTCP(port, siteFactory)
            reactor.run(**reactor_args)

    return run_app
