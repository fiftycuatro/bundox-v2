# -*- coding: utf-8 -*-
"""Stackoverflow Data Dumps

This module provides utilities for handling the stackoverflow data dumps. 

"""

import os
import io
import re
import lxml.etree as etree
import argparse
import shutil
import logging
import tempfile
import sqlite3
import json
from utilities import fast_iter, file_len, es_ensure_index, es_ensure_templates, es_bulk_add
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Q
from utilities import load_static_files
import time

logger = logging.getLogger(__name__)

ES_INDEX_FMT = 'bundox-stackoverflow-{0}'

POST_QUESTION = '1'
POST_ANSWER = '2'

tag_pattern = re.compile(r'\<([^\<\>]+)\>')


def parse_tags(tags):
    return [tag for tag in re.findall(tag_pattern, tags)]


def question_to_dict(row):
    question = {
        'id': int(row['Id']),
        'title': row['Title'],
        'body': row['Body'],
        'creationDate': row['CreationDate'],
        'score': int(row['Score']),
        'viewCount': int(row['ViewCount']),
        'tags': parse_tags(row.get('Tags', '')),
    }
    if 'OwnerDisplayName' in question:
        question['userDisplayName'] = row['OwnerDisplayName']
    if 'AcceptedAnswerId' in row:
        question['acceptedAnswerId'] = int(row['AcceptedAnswerId'])
    return question


def answer_to_dict(row):
    answer = {
        'id': int(row['Id']),
        'body': row['Body'],
        'creationDate': row['CreationDate'],
        'score': int(row['Score']),
        'tags': parse_tags(row.get('Tags', '')),
        'parentId': int(row['ParentId']),
    }
    if 'OwnerDisplayName' in answer:
        answer['userDisplayName'] = row['OwnerDisplayName']
    return answer


def comment_to_dict(row):
    comment = {
        'id': int(row['Id']),
        'postId': int(row['PostId']),
        'text': row['Text'],
        'creationDate': row['CreationDate'],
        'score': int(row['Score']),
    }
    if 'UserDisplayName' in row:
        comment['userDisplayName'] = row['UserDisplayName']
    return comment


def import_dump(es_addr, dump_path):
    es = Elasticsearch([es_addr])

    # TODO: Possibly just do import users into a dictionary for username

    specs = {
        'questions': {'fname': 'Posts', 'map': question_to_dict, 'filter': lambda row: row.get('PostTypeId', None) == POST_QUESTION},
        'answers': {'fname': 'Posts', 'map': answer_to_dict, 'filter': lambda row: row.get('PostTypeId', None) == POST_ANSWER},
        'comments': {'fname': 'Comments', 'map': comment_to_dict, 'filter': lambda row: len(row.keys()) > 0}
    }

    # TODO: import templates
    es_ensure_templates(
        es, ['stackoverflow-posts', 'stackoverflow-comments'], delete=True, logger=logger)

    for spec_name in specs:
        fname = specs[spec_name]['fname']
        filter_fn = specs[spec_name]['filter']
        map_fn = specs[spec_name]['map']
        with open(os.path.join(dump_path, fname + '.xml')) as xml_file:
            tree = etree.iterparse(xml_file)
            index = ES_INDEX_FMT.format(spec_name)
            es_ensure_index(es, index=index, delete=True)

            docs = []
            for count, row in enumerate(fast_iter(tree)):
                if filter_fn(row.attrib):
                    docs = es_bulk_add(es, index, docs, doc=map_fn(row.attrib))

                if count % 100000 == 0:
                    logger.info(
                        "Importing {0}: processed {1} rows".format(spec_name, count))

            # Flush any remaining
            es_bulk_add(es, index, docs, flush=True)

            logger.info(
                "Importing {0} complete: processed {1} rows".format(spec_name, count))


def create_dump_sample(src, dst, count):
    '''Creates a sample of an existing dump. Used to create a more managable data set for testing. 

    '''

    names = ['Posts', 'Users', 'Comments', 'Tags']
    dumps = dict(
        zip(names, [os.path.sep.join((src, '%s.xml' % n)) for n in names]))
    samps = dict(
        zip(names, [os.path.sep.join((dst, '%s.xml' % n)) for n in names]))

    xml_header = u'<?xml version="1.0" encoding="utf-8"?>\n'

    postids = set()
    userids = set()

    # Posts
    total_posts = file_len(dumps['Posts']) - 3
    count = min(total_posts, count)
    logger.info("Creating sample of size {0}".format(count))
    with open(dumps['Posts'], 'rb') as dump, io.open(samps['Posts'], 'w', encoding='utf-8') as samp:
        samp.write(xml_header)
        samp.write(u'<posts>\n')
        for row in fast_iter(etree.iterparse(dump), count=count):
            samp.write(etree.tostring(row, encoding='unicode'))
            postids.add(row.attrib['Id'])
            for k in ['OwnerUserId', 'LastEditorUserId']:
                if k in row.attrib:
                    userids.add(row.attrib[k])
        samp.write(u'</posts>')

    # Comments
    with open(dumps['Comments'], 'rb') as dump, io.open(samps['Comments'], 'w', encoding='utf-8') as samp:
        samp.write(xml_header)
        samp.write(u'<comments>\n')
        for row in fast_iter(etree.iterparse(dump)):
            if row.attrib.get('PostId', None) in postids:
                samp.write(etree.tostring(row, encoding='unicode'))
                if 'UserId' in row.attrib:
                    userids.add(row.attrib['UserId'])
        samp.write(u'</comments>\n')

    # Users
    with open(dumps['Users'], 'rb') as dump, io.open(samps['Users'], 'w', encoding='utf-8') as samp:
        samp.write(xml_header)
        samp.write(u'<users>\n')
        for row in fast_iter(etree.iterparse(dump)):
            if row.attrib.get('Id', None) in userids:
                samp.write(etree.tostring(row, encoding='unicode'))
        samp.write(u'</users>')

    # Tags
    # Just straight copy for now
    shutil.copyfile(dumps['Tags'], samps['Tags'])


def _expand_question(es_addr, question):
    es = Elasticsearch([es_addr])
    index = ES_INDEX_FMT.format('answers')

    if 'id' not in question:
        logger.warn('Could not find id in %r' % question)
        return

    # Get comments
    question['comments'] = comments(es_addr, post_id=question['id'])

    # Get answer with comments
    def add_comments(answer):
        answer['comments'] = comments(es_addr, post_id=answer['id'])
        return answer
    question['answers'] = map(add_comments, answers(
        es_addr, question_id=question['id']))

    return question


def questions(es_addr, id=None, search="", tags=[], size=50, page=1):
    _logger = logging.getLogger(__name__ + ".questions")
    es = Elasticsearch([es_addr])
    index = ES_INDEX_FMT.format('questions')

    if search is None:
        search = ""
    if tags is None:
        tags = []

    query = Q('bool', should=[
              Q('match', body=search), Q('match', title=search)])
    if len(tags) > 0:
        query += Q('bool', filter=Q('terms', tags=tags))

    if id is not None:
        question = es.get(index=index, doc_type="_all",
                          id=id).get('_source', {})
        return _expand_question(es_addr, question)
    else:
        s = Search(using=es, index=index).query(query)

        _logger.info(s.to_dict())
        start = (max(page, 1) - 1) * size
        end = start + size
        logger.info("Start {0}, End {1}".format(start, end))
        return [hit.to_dict() for hit in s[start:end].execute()]


def answers(es_addr, id=None, question_id=None):
    _logger = logging.getLogger(__name__ + ".answers")
    es = Elasticsearch([es_addr])
    index = ES_INDEX_FMT.format('answers')

    if id is not None:
        return es.get(index=index, doc_type="_all", id=id)
    else:
        s = Search(using=es, index=index)
        s = s.filter('term', parentId=question_id)
        _logger.info(s.to_dict())
        return [hit.to_dict() for hit in s.execute()]


def comments(es_addr, id=None, post_id=None):
    _logger = logging.getLogger(__name__ + ".comments")
    es = Elasticsearch([es_addr])
    index = ES_INDEX_FMT.format('comments')

    if id is not None:
        if isinstance(id, list):
            pass
        else:
            return es.get(index=index, doc_type="_all", id=id)
    else:
        s = Search(using=es, index=index)
        s = s.filter('term', postId=post_id)
        _logger.info(s.to_dict())
        return [hit.to_dict() for hit in s.execute()]
