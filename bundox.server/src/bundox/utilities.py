import os, pkg_resources, shutil, tempfile, posixpath
from datetime import datetime
import logging
from logging import Formatter
import logging.config
from copy import copy
from contextlib import contextmanager
from itertools import islice
import lxml.etree as etree
import yaml

from colorama import init, Fore, Back, Style

## -- Filesystem Utilities ----
##

def ensure_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)

def ensure_no_dir(path):
    if os.path.exists(path):
        shutil.rmtree(path)

def ensure_empty_dir(path):
    ensure_no_dir(path)
    ensure_dir(path)

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

@contextmanager
def TemporaryDirectory():
    name = tempfile.mkdtemp()
    try:
        yield name
    finally:
        shutil.rmtree(name)

def load_static_files(path):
    resource_package = __name__                 # Could be any module/package name
    resource_path = '/'.join(('static', path))  # Do not use os.path.join()
    return pkg_resources.resource_string(resource_package, resource_path)

def file_opener(filename):
    return lambda: (
        open(filename, 'rb'),
        datetime.utcfromtimestamp(os.path.getmtime(filename)),
        int(os.path.getsize(filename))
        )

def get_file_loader(filename):
    return lambda x: (os.path.basename(filename), file_opener(filename))

def get_package_loader(package, package_path):
    from pkg_resources import DefaultProvider, ResourceManager, get_provider
    loadtime = datetime.utcnow()
    provider = get_provider(package)
    manager = ResourceManager()
    filesystem_bound = isinstance(provider, DefaultProvider)

    def loader(path):
        if path is None:
            return None, None
        path = posixpath.join(package_path, path)
        if not provider.has_resource(path):
            return None, None
        basename = posixpath.basename(path)
        if filesystem_bound:
            return basename, file_opener(
                provider.get_resource_filename(manager, path))
        return basename, lambda: (
            provider.get_resource_stream(manager, path),
            loadtime,
            0
        )
    return loader

def get_directory_loader(directory):
    def loader(path):
        if path is not None:
            path = os.path.join(directory, path)
        else:
            path = directory
        if os.path.isfile(path):
            return os.path.basename(path), file_opener(path)
        return None, None
    return loader

## -- Logging Utilities ----
##

def setup_logging(
    default_path='logging.yaml',
    default_level=logging.INFO,
    env_key='LOG_CFG'
):
    """Setup logging configuration

    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        # Get static logging file
        try:
            config_text = load_static_files('logging.yaml')
            config = yaml.safe_load(config_text)
            logging.config.dictConfig(config)
        except Exception, e:
            import sys
            sys.stderr.write(str(e))
            logging.basicConfig(level=default_level)

class ColoredFormatter(Formatter):
    MAPPING = {
        'DEBUG'   : Fore.WHITE,           # 37, # white
        'INFO'    : Fore.CYAN,            # 36, # cyan
        'WARNING' : Fore.YELLOW,          # 33, # yellow
        'ERROR'   : Fore.RED,             # 31, # red
        'CRITICAL': Fore.WHITE + Back.RED # 41, # white on red bg
    }

    def __init__(self, format):

        # This simple call will allow for ANSI sequences to be 
        # stripped out of stdout/stderr on windows platform and
        # replaced with the appropriate Win32 class. Pretty neat!
        init()
        Formatter.__init__(self, format)

    def format(self, record):
        colored_record = copy(record)
        levelname = colored_record.levelname
        seq = ColoredFormatter.MAPPING.get(levelname, Fore.WHITE)
        colored_levelname = '{0}{1}{2}'.format(seq, levelname, Style.RESET_ALL)
        colored_record.levelname = colored_levelname

        return Formatter.format(self, colored_record)
        
## -- XML Utilities ----
##

def fast_iter(context, count=None, aggressive=False):
    """
    http://lxml.de/parsing.html#modifying-the-tree
    Based on Liza Daly's fast_iter
    http://www.ibm.com/developerworks/xml/library/x-hiperfparse/
    See also http://effbot.org/zone/element-iterparse.htm
    
    """
    tree = context if count is None else islice(context, count)
    for event, elem in tree:
        yield elem
        # It's safe to call clear() here because no descendants will be
        # accessed
        elem.clear()
        # Also eliminate now-empty references from the root node to elem
        if aggressive:
            # This seems much slower for me and only reduces memory by small amounts
            for ancestor in elem.xpath('ancestor-or-self::*'):
                while ancestor.getprevious() is not None:
                    del ancestor.getparent()[0]
        else:
            while elem.getprevious() is not None:
                del elem.getparent()[0]
    del context


## -- HTTP Utilities ----
##
import mimetypes
from time import time, mktime
from zlib import adler32
from werkzeug.routing import Rule, NotFound
from werkzeug.datastructures import Headers
from werkzeug.filesystem import get_filesystem_encoding
from werkzeug.http import is_resource_modified, http_date
from werkzeug._compat import string_types

def GET(*args, **kwargs):
    kwargs['methods'] = ['GET']
    return Rule(*args, **kwargs)

def DELETE(*args, **kwargs):
    kwargs['methods'] = ['DELETE']
    return Rule(*args, **kwargs)

def POST(*args, **kwargs):
    kwargs['methods'] = ['POST']
    return Rule(*args, **kwargs)

def PUT(*args, **kwargs):
    kwargs['methods'] = ['PUT']
    return Rule(*args, **kwargs)

class FileWrapper(object):
    def __init__(self, file, buffer_size=8192):
        self.file = file
        self.buffer_size = buffer_size

    def close(self):
        if hasattr(self.file, 'close'):
            self.file.close()

    def __iter__(self):
        return self

    def next(self):
        data = self.file.read(self.buffer_size)
        if data:
            return data
        raise StopIteration()

def wrap_file(environ, file, buffer_size=8192):
    return environ.get('wsgi.file_wrapper', FileWrapper)(file, buffer_size)
 
def generate_etag(mtime, file_size, real_filename):
    if not isinstance(real_filename, bytes):
        real_filename = real_filename.encode(get_filesystem_encoding())
    return 'wzsdm-%d-%s-%s' % (
        mktime(mtime.timetuple()),
        file_size,
        adler32(real_filename) & 0xffffffff
        )

def static_file_resource(path, directory=None, package=None, cache=False, cacheTimeout=60*60*12, defaultType='text/plain'):
    if package is not None:
        loader = get_package_loader(*package)
    elif isinstance(path, string_types):
        if directory is None:
            loader = get_file_loader(path)
        else:
            loader = get_directory_loader(directory)
    else:
        raise TypeError('unknown def %r' % path)
    
    real_filename, file_loader = loader(path)
    if real_filename is None:
        raise NotFound
    def response(environ, start_response):
        guessed_type = mimetypes.guess_type(real_filename)
        mime_type = guessed_type[0] or defaultType
        f, mtime, file_size = file_loader()
        
        headers = [('Content-Type', mime_type),
                   ('Content-Length', str(file_size))]

        if cache:
            timeout = cacheTimeout
            etag = generate_etag(mtime, file_size, real_filename)
            headers += [
                ('Etag', '"%s"' % etag),
                ('Cache-Control', 'max-age=%d, public' % timeout)
            ]
            if not is_resource_modified(environ, etag, last_modified=mtime):
                f.close()
                start_response('304 Not Modified', headers)
                return []
            headers.append(('Expires', http_date(time() + timeout)))
        else:
            headers.append(('Cache-Control', 'public'))
        start_response('200 OK', headers)
        return wrap_file(environ, f)

    return response

class CORSMiddleware(object):
    """Add Cross-origin resource sharing headers to every request.
    
    """

    def __init__(self, app, origin):
        self.app = app
        self.origin = origin

    def __call__(self, environ, start_response):

        def add_cors_headers(status, headers, exc_info=None):
            headers = Headers(headers)
            headers.add("Access-Control-Allow-Origin", self.origin)
            headers.add("Access-Control-Allow-Headers", "Origin, Content-Type")
            headers.add("Access-Control-Allow-Credentials", "true")
            headers.add("Access-Control-Allow-Methods", "GET, POST, PUT, etc")
            headers.add("Access-Control-Expose-Headers", "...")
            return start_response(status, headers.to_list(), exc_info)

        if environ.get("REQUEST_METHOD") == "OPTIONS":
            add_cors_headers("200 Ok", [("Content-Type", "application/json")])
            return [b'200 Ok']

        return self.app(environ, add_cors_headers)

## -- Elasticsearch Utilities ----
##
import json
from elasticsearch.helpers import bulk

def get_elasticsearch_conn():
   pass

class ConnectionError(Exception):
    pass

def ensure_elasticsearch(es, logger=None):
    logger = logger or logging.getLogger(__name__)
    if not es.ping():
        raise ConnectionError("Elasticsearch does not seem to be up") 
    logger.info("Elasticsearch up")

def es_ensure_index(es, index, delete=False):
    if delete:
        es.indices.delete(index=index, ignore=404)
    es.indices.create(index, ignore=400)

def es_ensure_templates(es, names, logger=None, delete=False):
    templates = json.loads(load_static_files('templates.json'))
    logger = logger or logging.getLogger(__name__)

    for name in names:
        tname = '.%s' % name
        if delete:
            es.indices.delete_template(name=tname, ignore=404)
        if not es.indices.exists_template(name=tname):
            logger.info("Loading template [%s]" % name)
            es.indices.put_template(name=tname, body=templates[name], create=True)

def es_bulk_add(es, index, docs, doc=None, flush=False, batch_size=500, id=None):
    if doc is not None:
        doc.update({
            '_index': index,
            '_type': '_doc',
            '_id': id if id is not None else doc['id']
        })
        docs.append(doc)
    if flush or len(docs) >= batch_size:
        bulk(es, docs)
        docs = []
    return docs