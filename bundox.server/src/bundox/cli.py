import sys
import logging
import utilities
import click
from flask import Flask

import docset
import stackoverflow
import http

# -- Global cli options ----
##
@click.group()
@click.option('-v', '--verbose', count=True, help='Logging level (1=WARNING, 2=INFO, 3=DEBUG)')
@click.option('--loggers', default='bundox', help='Comma seperated loggers to set level for')
def cli(verbose, loggers):
    utilities.setup_logging()
    logging.getLogger().setLevel(logging.CRITICAL)
    if verbose > 0:
        levels = {1: logging.WARNING,
                  2: logging.INFO,
                  3: logging.DEBUG}

        # Set levels only for specified loggers
        for name in loggers.split(','):
            logging.getLogger(name.strip()).setLevel(
                levels.get(verbose, logging.DEBUG))

# -- Docset Commands ----
##
@cli.group(name='docset')
def docset_cli():
    pass


@docset_cli.command(name='add')
@click.option('--es_addr', default='http://localhost:9200', help='Elasticsearch Address')
@click.option('--name', default=None, help='Docset name')
@click.option('--version', default=None, help='Docset version')
@click.option('--data', default=None, help='Data directory')
@click.option('--force', default=False, is_flag=True, help='Force overwrite existing')
@click.argument('docset_file')
def docset_add(es_addr, data, docset_file, name, version, force):
    docset.add(es_addr, data, docset_file, name, version, force)


@docset_cli.command(name='delete')
@click.option('--es_addr', default='http://localhost:9200', help='Elasticsearch Address')
@click.option('--name', default=None, help='Docset name')
@click.option('--version', default=None, help='Docset version')
@click.option('--data', default=None, help='Data directory')
def docset_delete(es_addr, data, name, version):
    docset.delete(es_addr, data, name, version)


@docset_cli.command(name='documents')
@click.option('--es_addr', default='http://localhost:9200', help='Elasticsearch Address')
@click.option('--name', default=None, help='Document Name')
@click.option('--version', default=None, help='Document Version')
def docset_documents(es_addr, name, version):
    for item in docset.documents(es_addr, name=name, version=version):
        click.echo(item)


@docset_cli.command(name='search')
@click.option('--es_addr', default='http://localhost:9200', help='Elasticsearch Address')
@click.option('--term', default=None, help='Search term')
def docset_find(es_addr, term):
    for item in docset.search(es_addr, searchTerm=term):
        click.echo(item)

# -- Stackoverflow Commands ----
##
@cli.group('stackoverflow')
def stackoverflow_cli():
    pass


@stackoverflow_cli.command()
@click.option('--src', help='Source folder of dump to sample')
@click.option('--dst', help='Destination folder to write sample dump')
@click.option('--count', type=click.INT, help='Total number of posts')
def create_sample(src, dst, count):
    stackoverflow.create_dump_sample(src, dst, count)


@stackoverflow_cli.command()
@click.option('--es_addr', default='http://localhost:9200', help='Elasticsearch Address')
@click.option('--dump_path', help='Stackoverflow dump path')
def import_dump(es_addr, dump_path):
    stackoverflow.import_dump(es_addr, dump_path)

# -- Start Webserver ----
##
@cli.command()
@click.option('--es_addr', default='http://localhost:9200', help='Elasticsearch Address')
@click.option('--data', default='./data', help='Docset data directory')
@click.option('--host', default='localhost', help='Server host')
@click.option('--port', default=8054, type=click.INT, help='Server port')
@click.option('--debug', default=False, type=click.BOOL, help='Debug mode')
def start(host, port, es_addr, data, debug):
    #http.app(host, port, es_addr=es_addr, data=data)
    app = http.create_app(host=host, port=port, debug=debug,
                          es_addr=es_addr, data_path=data)
    app()


# -- Main entrypoint ----
##


def cli_main():
    try:
        cli()
    except Exception, e:
        logger = logging.getLogger(__name__)
        logger.critical(e, exc_info=True)


if __name__ == '__main__':
    cli_main()
