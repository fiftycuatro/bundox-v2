import sys
import os
import pkg_resources
import json
import logging
import shutil
import tarfile
import plistlib
import sqlite3
import tempfile
from urllib import unquote_plus
from urllib2 import urlopen
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Q
from collections import namedtuple
from bs4 import BeautifulSoup
from utilities import load_static_files, TemporaryDirectory, ensure_no_dir, \
    ensure_empty_dir, ensure_elasticsearch, es_ensure_index, es_bulk_add, \
    es_ensure_templates

logger = logging.getLogger(__name__)

import time
# -- CONSTANTS -----
##

BUNDOX_INDEX = 'bundox-documents'
DOCSET_INDEX_FORMAT = 'bundox-docset-%s'
ALL_DOCSETS_INDEX = DOCSET_INDEX_FORMAT % '*'
DOCSET_PATH_FORMAT = '%s.docset'
DOC_TYPE = '_doc'
PLIST_REL_PATH = os.path.sep.join(('Contents', 'Info.plist'))
DSIDX_REL_PATH = os.path.sep.join(('Contents', 'Resources', 'docSet.dsidx'))
DOCS_REL_PATH  = os.path.sep.join(('Contents', 'Resources', 'Documents'))

# -- VALUE TYPES -----
##

Document = namedtuple(
    'Document', 'id name version family indexpath, formatfamily')
DocumentationItem = namedtuple(
    'DocumentationItem', 'subject path type namespace documentId')

# -- DOCSET ----
##


def docset_id(name, version):
    return ("%s_%s" % (name, version)).lower()

def docset_index(name, version):
    return DOCSET_INDEX_FORMAT % (docset_id(name, version).lower())

def docset_path(base, name, version):
    return DOCSET_PATH_FORMAT % (os.path.sep.join((base, 'docsets', docset_id(name, version))))

def plist_path(dst):
    return os.path.sep.join((dst, PLIST_REL_PATH))

def dsidx_path(dst):
    return os.path.sep.join((dst, DSIDX_REL_PATH))

def document_path(dst):
    return os.path.sep.join((dst, DOCS_REL_PATH))

def resolve_namespace(base, document, subject, path, type):
    # This was too slow. 
    #def resolve_default(base, document, subject, path, type):
    #    dst = docset_path(base, document.name, document.version)
    #    doc_path = "/".join((document_path(dst), path)).split('#')[0]
    #    with open(doc_path, 'r') as f:
    #        doc = BeautifulSoup(f, features="lxml")
    #        return doc.title.string
    
    def resolve_default(base, document, subject, path, type):
        return ""

    def resolve_java(base, document, subject, path, type):
        return '.'.join(path.rsplit('.html', 1)[0].split('/')[:-1])
    resolvers = dict(java=resolve_java)
    return resolvers.get(document.formatfamily, resolve_default)(base, document, subject, path, type)

# -- DB Schemas ----
##

def ResultIter(cursor, arraysize=1000):
    'An iterator that uses fetchmany to keep memory usage down'
    while True:
        results = cursor.fetchmany(arraysize)
        if not results:
            break
        for result in results:
            yield result

def dashFormatRows(conn):
    query = "SELECT * FROM searchIndex"
    cur = conn.cursor()
    cur.execute(query)
    for _, tokenname, typename, path, in ResultIter(cur):
        yield (tokenname, path, typename)


def zFormatRows(conn):
    query = """SELECT ZTOKENNAME,ZPATH,ZANCHOR,ZTYPENAME FROM ZTOKEN as t 
            JOIN ZTOKENMETAINFORMATION as m ON t.ZMETAINFORMATION = m.Z_PK 
            JOIN ZFILEPATH as f ON m.ZFILE = f.Z_PK 
            JOIN ZTOKENTYPE as n ON t.ZTOKENTYPE = n.Z_PK"""
    cur = conn.cursor()
    cur.execute(query)
    for name, path, anchor, typename in ResultIter(cur):
        if path.startswith('<dash_entry_name='):
            parts = path.split('>')
            path = parts[-1]
            name = parts[0][len('<dash_entry_name='):] 
            if anchor is not None:
                path = path + "#" + anchor 
            path = unquote_plus(path)
            yield (name, path, typename)
        else:
            yield (name, '%s#%s' % (path, anchor), typename)


def docsetRows(dbpath):
    conn = sqlite3.connect(dbpath, isolation_level=None)
    try:
        cur = conn.cursor()
        cur.execute(
            "SELECT COUNT(*) FROM sqlite_master WHERE name='searchIndex'")
        dashFormat = cur.fetchone()[0] != 0

        if dashFormat:
            for row in dashFormatRows(conn):
                yield row
        else:
            for row in zFormatRows(conn):
                yield row
    finally:
        conn.close()


def name_value_from_args(docset_path, name, version):
    basename = os.path.basename(docset_path)
    filename, extension = os.path.splitext(basename)
    parts = filename.rsplit('_', 1)
    return (name or parts[0], version or parts[1])


def extract_docset(src, dst):
    logger.info("Extracting Docset")
    with TemporaryDirectory() as tmp:
        ensure_no_dir(dst)
        with tarfile.open(src, 'r:gz') as tar:
            tar.extractall(tmp)
        inner_src = os.path.sep.join((tmp, next(os.walk(tmp))[1][0]))
        shutil.move(inner_src, dst)
    logger.info("Docset Extraction Complete")


# -- Elasticsearch Query DSL -----
##


def documents(es_addr, name=None, version=None, size=200):
    es = Elasticsearch([es_addr])
    ensure_elasticsearch(es, logger=logger)

    s = Search(using=es, index=BUNDOX_INDEX)
    if name is not None:
        s = s.query('term', name=name)
    if version is not None:
        s = s.query('term', version=version)
    return [hit.to_dict() for hit in s]

def delete(es_addr, data, name, version):
    es = Elasticsearch([es_addr])
    ensure_elasticsearch(es, logger=logger)

    id = docset_id(name, version)
    index = docset_index(name, version)
    path = docset_path(data, name, version)

    es.delete(index=BUNDOX_INDEX, id=id, doc_type=DOC_TYPE, ignore=404)
    es.indices.delete(index=index, ignore=404)
    ensure_empty_dir(path)

    # TODO: more to come

def search(es_addr, searchTerm=None, indices=ALL_DOCSETS_INDEX, size=200):
    es = Elasticsearch([es_addr])
    ensure_elasticsearch(es, logger=logger)

    searchTerm = searchTerm.lower()

    def wildcardify(s):
        return s.replace('', '*')

    s = Search(using=es, index=indices)

    if searchTerm is not None:
        s = s.query(
            Q('bool',
              must=Q('wildcard', subject=wildcardify(searchTerm)),
              should=[Q('term', subject={'value': searchTerm, 'boost': 10}),
                      Q('prefix', subject={'value': searchTerm, 'boost': 2}),
                      Q('wildcard', subject=wildcardify(searchTerm)[1:])]))

    
    logger.info(s.to_dict())
    results = [hit.to_dict() for hit in s[0:size].execute()]

    docs = {d['id']: d for d in documents(es_addr)}
    def addDocument(r):
        r['document'] = docs[r['documentId']]
        return r
    
    return [addDocument(r) for r in results]


def add(es_addr, data, docset, name, version, force):
    es = Elasticsearch([es_addr])

    ensure_elasticsearch(es, logger=logger)
    es_ensure_templates(es, ['bundox', 'docset'], logger=logger)
    es_ensure_index(es, index=BUNDOX_INDEX)


    # Allow docset to be a url to a docset file
    isTempDocset = False
    try:
        f = urlopen(docset)
        with tempfile.NamedTemporaryFile(delete=False) as tmp:
            docset = tmp.name
            tmp.write(f.read())
        f.close()
        isTempDocset = True
    except ValueError:  # not a URL
        pass

    try:
        # Get Name, Version of Docset
        name, version = name_value_from_args(docset, name, version)

        if name is None or version is None:
            logger.error("Cannot determine name and version")

        id = docset_id(name, version)
        path = docset_path(data, name, version)
        index = docset_index(name, version)

        # Check to see if docset exists
        if es.exists(index=BUNDOX_INDEX, doc_type=DOC_TYPE, id=id):
            if force:
                delete(es_addr, data=data, name=name, version=version)
            else:
                raise ValueError(
                    "Docset %s already exists. Please use -f,--force to override" % docset_id)

        # Extract docset
        extract_docset(docset, path)

        # Get Docset Info
        plist = plistlib.readPlist(plist_path(path))
        family = plist.get('DocSetPlatformFamily', 'unknown')
        indexpath = plist.get('dashIndexFilePath', '')
        formatfamily = plist.get('DashDocSetFamily', 'unknown')

        # Add document to all documents
        document = Document(id, name, version, family, indexpath, formatfamily)
        es.create(index=BUNDOX_INDEX, doc_type=DOC_TYPE,
                  id=id, body=document._asdict())

        # Import docset
        es_ensure_index(es, index=index)
        bulkdocs = []
        for subject, path, entry_type in docsetRows(dsidx_path(path)):
            namespace = resolve_namespace(
                data, document, subject, path, entry_type)
            item = DocumentationItem(
                subject, path, entry_type, namespace, document.id)
            #es.index(index=index, doc_type=DOC_TYPE, body=item._asdict())
            bulkdocs = es_bulk_add(es, index, bulkdocs, id=subject, doc=item._asdict())

        # Flush the remaining items 
        es_bulk_add(es, index, bulkdocs, flush=True)
    finally:
        if isTempDocset:
            try:
                os.remove(docset)
            except:
                pass


