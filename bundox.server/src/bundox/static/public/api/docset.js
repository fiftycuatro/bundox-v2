export async function getDocsets(name=null, version=null) {
    const url = "/api/v1/documents";
    const response = await axios.get(url);
    return response.data;
}

export async function searchDocsetsDocumentation(query) {
    // Can't query without a search term
    if (!query)
        return []

    const url = `/api/v1/documents/documentation?searchTerm=${query}`;
    const response = await axios.get(url);
    return response.data;
}