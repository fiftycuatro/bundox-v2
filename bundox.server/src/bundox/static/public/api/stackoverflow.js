import { stringify } from 'Qs';

export async function searchQuestions(text) {
    const url = "/api/v2/questions/search";
    const params = {}

    const parsed = stackoverflow_parser.parse(text);

    if (parsed.keywords.length > 0 || parsed.phrases.length > 0) {
        params.search = parsed.keywords.join(' ') + parsed.phrases.join(' ');
    }
    if (parsed.tags.length > 0) {
        params.tag = parsed.tags
    }

    console.log(params)

    const response = await axios.get(url, {
        params,
        paramsSerializer: params => {
            return stringify(params, { indices: false })
        }
    });
    return response.data;
}

export async function getQuestion({ id = false }) {
    if (!id)
        return {}
    const url = `/api/v2/questions/${id}`;
    const response = await axios.get(url);
    return response.data;
}