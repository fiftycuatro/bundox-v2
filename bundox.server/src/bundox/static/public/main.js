import { useState, useEffect } from 'React';
import { BrowserRouter as Router, Route, Link } from 'ReactRouterDOM';
import { useFormInput, useDebounce } from './components/hooks.js'

import Layout from './components/Layout.js'
import SplitPane, { SplitPaneContainer } from './components/SplitPane.js'
import DocsetList from './components/DocsetList.js'
import SearchResultsList from './components/SearchResultsList.js'
import DocsetViewport from './components/DocsetViewport.js'

import StackoverflowResults from './components/StackoverflowResults'
import StackoverflowViewport from './components/StackoverflowViewport'

import * as docsetAPI from './api/docset.js'
import * as stackoverflowAPI from './api/stackoverflow.js'

const SearchInput = styled.input`
    width: 100%;
    box-sizing: border-box;
`;

const DocsetSideBar = () => {
    const [docsets, setDocsets] = useState([]);
    const [searchResults, setSearchResults] = useState([]);
    const [searchInput, onSearchInputChange] = useFormInput('');
    const debouncedSearchInput = useDebounce(searchInput, { minLength: 3 });

    async function fetchDocsets(name = null, version = null) {
        setDocsets(await docsetAPI.getDocsets(name, version))
    }

    async function searchDocsets(name = null, version = null) {
        setSearchResults(await docsetAPI.searchDocsetsDocumentation(name, version))
    }

    // Load all docsets on page load
    useEffect(() => { fetchDocsets() }, []);

    // Search when search input is debounced
    useEffect(() => {
        if (debouncedSearchInput) {
            searchDocsets(debouncedSearchInput)
        } else {
            // clear results
            setSearchResults([]);
        }
    }, [debouncedSearchInput])

    const SideBarList = debouncedSearchInput ?
        <SearchResultsList results={searchResults} /> :
        <DocsetList docsets={docsets} />

    return (
        <React.Fragment>
            <SearchInput value={searchInput} onChange={onSearchInputChange} />
            {SideBarList}
        </React.Fragment>
    );
}

const StackoverflowSideBar = () => {
    const [searchResults, setSearchResults] = useState([]);
    const [searchInput, onSearchInputChange] = useFormInput('');
    const debouncedSearchInput = useDebounce(searchInput, { minLength: 3 });

    async function searchQuestions(args) {
        console.log(args)
        setSearchResults(await stackoverflowAPI.searchQuestions(args))
    }

    // Search when search input is debounced
    useEffect(() => {
        if (debouncedSearchInput) {
            // TODO: Parse query string
            searchQuestions(debouncedSearchInput)
        } else {
            // clear results
            setSearchResults([]);
        }
    }, [debouncedSearchInput])

    const SideBarList = debouncedSearchInput ?
        <StackoverflowResults questions={searchResults} /> :
        <div />

    return (
        <React.Fragment>
            <SearchInput value={searchInput} placeholder="Search Query" onChange={onSearchInputChange} />
            {SideBarList}
        </React.Fragment>
    );
}

const Welcome = () => {
    return (
        <div>
            <h1>Welcome to Bundox v2</h1>
            <ul>
                <li><Link to="/docset">Search Docsets</Link></li>
                <li><Link to="/stackoverflow">Search Stackoverflow Questions</Link></li>
            </ul>
        </div>
    );
};

// Main Application Root
export default function App(props) {
    return (
        <Router>
            <Layout>
                <SplitPane sizes={[25, 75]}>
                    <SplitPaneContainer>
                        <Route path="/docset" component={DocsetSideBar} />
                        <Route path="/stackoverflow" component={StackoverflowSideBar} />
                    </SplitPaneContainer>
                    <SplitPaneContainer>
                        <Route exact path="/" component={Welcome} />
                        <Route path="/docset/documentation/:name/:version/:path+" component={DocsetViewport} />
                        <Route path="/stackoverflow/question/:id" component={StackoverflowViewport} />
                    </SplitPaneContainer>
                </SplitPane>
            </Layout>
        </Router>

    );
}

