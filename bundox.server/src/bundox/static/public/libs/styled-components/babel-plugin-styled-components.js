/**
 * Minified by jsDelivr using Terser v3.14.1.
 * Original file: /npm/babel-plugin-styled-components@1.10.6/lib/index.js
 * 
 * Do NOT use SRI with dynamically generated files! More information: https://www.jsdelivr.com/using-sri-with-dynamic-files
 */
"use strict"; Object.defineProperty(exports, "__esModule", { value: !0 }), exports.default = _default; var _babelPluginSyntaxJsx = _interopRequireDefault(require("babel-plugin-syntax-jsx")), _pure = _interopRequireDefault(require("./visitors/pure")), _minify = _interopRequireDefault(require("./visitors/minify")), _displayNameAndId = _interopRequireDefault(require("./visitors/displayNameAndId")), _templateLiterals = _interopRequireDefault(require("./visitors/templateLiterals")), _assignStyledRequired = _interopRequireDefault(require("./visitors/assignStyledRequired")), _transpileCssProp = _interopRequireDefault(require("./visitors/transpileCssProp")); function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e } } function _default(e) { var r = e.types; return { inherits: _babelPluginSyntaxJsx.default, visitor: { Program(e, i) { e.traverse({ JSXAttribute(e, i) { (0, _transpileCssProp.default)(r)(e, i) }, VariableDeclarator(e, i) { (0, _assignStyledRequired.default)(r)(e, i) } }, i) }, CallExpression(e, i) { (0, _displayNameAndId.default)(r)(e, i), (0, _pure.default)(r)(e, i) }, TaggedTemplateExpression(e, i) { (0, _minify.default)(r)(e, i), (0, _displayNameAndId.default)(r)(e, i), (0, _templateLiterals.default)(r)(e, i), (0, _pure.default)(r)(e, i) } } } }
//# sourceMappingURL=/sm/49a55fa387c378742cee3a6bf0496782f7baa9ad4d78330b4081627d01564e3b.map

