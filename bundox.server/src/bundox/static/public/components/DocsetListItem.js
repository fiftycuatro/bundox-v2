import { Link } from 'ReactRouterDOM';

export default function DocsetListItem({ name, version, indexpath}) {
    return (
        <li>
            <Link to={`/docset/documentation/${name}/${version}/${indexpath}`}>{`${name} - ${version}`}</Link>
        </li>
    );
}