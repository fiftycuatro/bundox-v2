const SplitPane = styled(ReactSplit)`
  div.gutter.gutter-horizontal {
    float: left;
    cursor: ew-resize;
    height: calc( 100vh - 50px );
    background: grey;
  }
`;

export const SplitPaneContainer = styled.div`
   float: left;
   width: 100%;
`;

export default SplitPane;
