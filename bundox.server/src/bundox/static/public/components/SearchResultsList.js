import DocsetSearchResultListItem from './DocsetSearchResultListItem.js'

const ScrollPane = styled.ul`
    width: 100%;
    height: calc( 100vh - 69px );
    overflow-y: scroll;
`;

export default function SearchResultsList( {results} ) {
    return (
        <ScrollPane>
            {results.length > 0 ?
                results.map((result) => (
                    <DocsetSearchResultListItem
                        key={`${result.documentId}_${result.path}`}
                        {...result} />
                ))
                : (<h1>No Results...</h1>)
            }
        </ScrollPane>
    )
}