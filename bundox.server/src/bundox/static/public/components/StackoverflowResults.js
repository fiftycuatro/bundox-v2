import { Link } from 'ReactRouterDOM';

const ScrollPane = styled.ul`
    width: 100%;
    height: calc( 100vh - 69px );
    overflow-y: scroll;
`;

const ListItem = styled.li`
   padding: 5px;
   border-bottom-style: solid;
   border-bottom-width: 1px;
   border-bottom-color: gray;
`;

const ResultItem = ({ id, title }) => {
    return (
        <ListItem>
            <Link to={`/stackoverflow/question/${id}`}>{title}</Link>
        </ListItem>
    );
}

export default function StackoverflowResults({ questions }) {
    console.log(questions);
    return (
        <ScrollPane>
            {questions.map((question) => (<ResultItem key={question.id} {...question} />))}
        </ScrollPane>
    );
};