import DocsetListItem from './DocsetListItem.js'

const ScrollPane = styled.ul`
    width: 100%;
    height: calc( 100vh - 69px );
    overflow-y: scroll;
`;

export default function DocsetList({ docsets }) {
    console.log(docsets);
    return (
        <ScrollPane>
            {docsets.map((docset) => (<DocsetListItem key={docset.id} {...docset} />))}
        </ScrollPane>
    );
};