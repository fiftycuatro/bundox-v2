
import { matchPath } from 'ReactRouter';

const IFrame = styled.iframe`
    width: 100%;
    height: calc( 100vh - 50px );
`;

export default function DocsetViewport({ location, match, history }) {
    const { name, version, path } = match.params;
    const { hash, search } = location;
    const url = `/api/v1/documents/${name}/${version}/documentation/${path}${hash}${search}`
    var iframeRef;

    function onClick(e) {
        const url = new URL(e.target.href)
        const path = `${url.pathname}${url.hash}${url.search}`
        const match = matchPath(path, {
            path: '/api/v1/documents/:name/:version/documentation/:path+'
        });

        if (match) {
            const { name, version, path } = match.params
            history.push(`/docset/documentation/${name}/${version}/${path}`)
        }
        e.preventDefault();
    }

    function onLoad(e) {
        var ref = iframeRef
        ref.contentWindow.addEventListener('click', onClick);
    }

    return (
        <IFrame innerRef={(el) => { iframeRef = el; }} src={url} onLoad={onLoad} />
    );
}