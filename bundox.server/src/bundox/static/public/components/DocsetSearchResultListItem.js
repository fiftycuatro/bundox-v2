import { Link } from 'ReactRouterDOM';

export default function DocsetSearchResultListItem({ path, subject, documentId }) {
    const linkPath = path
    const parts = documentId.split('_');
    const name = parts[0];
    const version = parts[1];

    return (
        <li>
            <Link to={`/docset/documentation/${name}/${version}/${path}`}>{subject}</Link>
        </li>
    );
}