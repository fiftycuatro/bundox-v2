import { useState, useEffect } from 'React';

export function useFormInput(initialValue) {
    const [value, setValue] = useState(initialValue);

    function handleChange(e) {
        setValue(e.target.value);
    }
    return [value, handleChange];
}

// Debounce input allowing for minLengths
export function useDebounce(value, { minLength = 0, delay = 500 }) {
    const [debouncedValue, setDebouncedValue] = useState(value);

    useEffect(() => {
        // Set to empty if minLength is not met
        if (value.length < minLength) {
            setDebouncedValue('');
            return;
        }
        
        // Otherwise set value when delay expires
        const handler = setTimeout(() => {
            setDebouncedValue(value);
        }, delay);

        // Clear setting if value changes inbetween
        return () => {
            clearTimeout(handler);
        };
    }, [value]);

    return debouncedValue;
}