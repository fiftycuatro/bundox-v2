import { Link, NavLink } from 'ReactRouterDOM';

const Header = styled.header`
  background: white;
  width: 100%;
  height: 50px;
  padding: 5px;
  box-sizing: border-box;

  display: flex;
  justify-content: space-between;

  border-bottom-style: solid;
  border-bottom-color: #ddd;
  border-bottom-width: 1px;

  h1 {
    font-size: 2em;
    font-weight: bold;
  }

  a {
    color: black;
    text-decoration: none;
  }

`;

const Nav = styled.nav`
  display: flex;
  align-items: center;

  a {
    height: 30px;
    line-height: 30px;
    padding: 0px 5px;
    margin-right: 10px;
    font-size: 1.25em;

    border-style: solid;
    border-color: #ddd;
    border-width: 1px;
    border-radius: 2.5px

    text-align: center;
    vertical-align: middle;
  }
  .active {
    background-color: #0eb2ff;
  }
`;

const Content = styled.section`
  height: calc( 100vh - 50px );
  width: 100%;
`;

const Splitter = styled(ReactSplit)`
  div.gutter.gutter-horizontal {
    float: left;
    cursor: ew-resize;
    height: calc( 100vh - 50px );
    background: grey;
  }
`;

const SplitContainer = styled.div`
   float: left;
`;

const Image = styled.img`
  height: 100%;
`;


export default function Layout(props) {
  return (
    <React.Fragment>
      <Header>
        <Link to="/">
          <h1>BUNDOX</h1>
        </Link>
        <Nav>
          <NavLink to="/docset">Docsets</NavLink>
          <NavLink to="/stackoverflow">Stackoverflow</NavLink>
        </Nav>
      </Header>
      <Content>
        {props.children}
      </Content>
    </React.Fragment>
  );
}
