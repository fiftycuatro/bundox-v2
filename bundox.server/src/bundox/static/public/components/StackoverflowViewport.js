import { useState, useEffect } from 'React';
import * as stackoverflowAPI from './api/stackoverflow.js'



const TitleWrapper = styled.div`
    width: 100%;
    
    h1 {
        line-height: 1.3;
        margin 0 0 1em;
    }
`;

const QuestionTitle = ({ children }) => {
    return (
        <TitleWrapper>
            <h1>{children}</h1>
        </TitleWrapper>
    )
};

const QuestionContainer = styled.div`
    width: 100%;
    height: calc( 100vh - 50px );
    box-sizing: border-box;

    h1 {
        font-size: 1.75em;
        font-weight: bold;
        margin-bottom: 10px;
    };

    overflow-y: auto;
    padding: 10px;
`;

const PostContainer = styled.div`
    margin-bottom: 10px;
    border-bottom-style: solid;
    border-bottom-width: 1px;
    border-bottom-color: black;

    h1 {
        font-size: 2em;
        font-weight: bold;
    };

    pre {
        margin-bottom: 1em;
        padding: 12px 8px;
        padding-bottom: 20px !ie7;
        width: auto;
        width: 650px !ie7;
        max-height: 600px;
        overflow: auto;
        font-family: Consolas,Menlo,Monaco,Lucida Console,Liberation Mono,DejaVu Sans Mono,Bitstream Vera Sans Mono,Courier New,monospace,sans-serif;
        font-size: 13px;
        background-color: #eff0f1;
        border-radius: 3px;
    };

    code {
        font-family: monospace;
    }
`;

const CommentList = styled.ul`

    list-style: circle
    display: block;
    margin-left: 20px;
    margin-bottom: 1em;
    padding: 12px 8px;
    padding-bottom: 20px !ie7;
    width: auto;
    max-height: 600px;
    font-size: 14px;
    background-color: #ffe5b5;
    border-radius: 3px;

    li {
        display: block;
        border-bottom-style: solid;
        border-bottom-width: 1px;
        border-bottom-color: #fdcb6e;
        padding-top: 10px;
    }
`;

const Comments = ({ comments }) => {
    if (comments.length == 0) {
        return (<React.Fragment />)
    }

    return (
        <CommentList>
            {comments.map((comment) => <li key={comment.id}>{comment.text}</li>)}
        </CommentList>
    )
};

const Answer = ({ answer }) => {
    return (
        <div>
            <PostContainer dangerouslySetInnerHTML={{ __html: answer.body }} />
            <Comments comments={answer.comments} />
        </div>
    )
};

const Answers = ({ answers }) => {
    return (
        <ul>
            {answers.map((answer) => <li key={answer.id}><Answer answer={answer} /></li>)}
        </ul >
    )
};

export default function StackoverflowViewport({ match }) {
    const [question, setQuestion] = useState();

    async function getQuestion(id) {
        setQuestion(await stackoverflowAPI.getQuestion({ id }))
    };

    console.log(match.params.id)

    // Load question
    useEffect(() => { getQuestion(match.params.id) }, [match.params.id]);

    if (!question) {
        return <h2>Loading...</h2>
    }

    const theDate = new Date();

    return (
        <QuestionContainer>
            <QuestionTitle>{question.title}</QuestionTitle>
            <PostContainer dangerouslySetInnerHTML={{ __html: question.body }} />
            <Comments comments={question.comments}></Comments>
            <Answers answers={question.answers}></Answers>
        </QuestionContainer>
    );
}